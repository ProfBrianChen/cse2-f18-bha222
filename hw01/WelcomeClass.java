/////////////////////////////
// BENJAMIN AULENBACH, BHA222
// CSE2, HW01
// Prints a welcome message

public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-B--H--A--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    
  }
}