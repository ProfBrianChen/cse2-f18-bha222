///////////////////////
//Benjamin Aulenbach 9/11/18
//Arithmetic.java
//STATE WHAT CODE DOES HERE

public class Arithmetic { 
  
  public static void main(String[] args){ //main method required for every Java program
    
    int numPants = 3;//Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    
    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt
    
    double paSalesTax = 0.06; //the tax rate

    //this block calculates total costs for each item before tax
    double totalCostPants = numPants * pantsPrice;
    double totalCostShirts = numShirts * shirtPrice;
    double totalCostBelts = numBelts * beltCost;
    
    //this block calculates just the sales tax for each item
    double salesTaxPants = totalCostPants * paSalesTax;
    double salesTaxShirts = totalCostShirts * paSalesTax;
    double salesTaxBelts = totalCostBelts * paSalesTax;
    
    //this block calculates totals of just items, just tax, and both together
    double subTotal = totalCostPants + totalCostShirts + totalCostBelts;
    double taxTotal = salesTaxPants + salesTaxShirts + salesTaxBelts;
    double finalTotal = subTotal + taxTotal;
    
    //this block takes all three total types, as well as tax totals, and rounds them to two decimal places
    subTotal = ((int)(subTotal*100))/100.0;
    taxTotal = ((int)(taxTotal*100))/100.0;
    finalTotal = ((int)(finalTotal*100))/100.0;
    salesTaxShirts = ((int)(salesTaxShirts*100))/100.0;
    salesTaxPants = ((int)(salesTaxPants*100))/100.0;
    salesTaxBelts = ((int)(salesTaxBelts*100))/100.0;
    
    //these blocks print out the information calculated
    System.out.println("Total Cost of Pants: $" + totalCostPants);
    System.out.println("Total Tax for Pants: $" + salesTaxPants);
    System.out.println("");
    
    System.out.println("Total Cost of Shirts: $" + totalCostShirts);
    System.out.println("Total Tax for Shirts: $" + salesTaxShirts);
    System.out.println("");
    
    System.out.println("Total Cost of Belts: $" + totalCostBelts);
    System.out.println("Total Tax for Belts: $" + salesTaxBelts);
    System.out.println("");
    
    System.out.println("Sub Total: $" + subTotal);
    System.out.println("Tax Total: $" + taxTotal);
    System.out.println("Total: $" + finalTotal);
    
  } //end of main mehtod
} //end of class