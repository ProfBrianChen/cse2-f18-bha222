///////////////////////
//Benjamin Aulenbach 9/18/18
//Convert.java
//CSE 002, BHA222
//Determines how much rain has fallen in cubic miles

import java.util.Scanner; //imports the scanner object used for input

public class Convert { //creates a class needed for the program
  
  public static void main(String[] args){ //main method needed for the program
    
    Scanner input = new Scanner(System.in); //creates a new scanner to accept input
    
    //initializes constant variables
    final double SQUARE_INCHES_PER_ACRE = 6.273e+6;
    final double GALLONS_PER_CUBIC_INCH = 0.004329;
    final double CUBIC_MILES_PER_GALLON = 9.0817e-13;
    
    //ask user for input on affected acreage
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = input.nextDouble();
    
    //ask user for input on inches
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = input.nextDouble();
    
    //calculates cubic miles of rain
    double gallons = affectedArea*SQUARE_INCHES_PER_ACRE*rainfall*GALLONS_PER_CUBIC_INCH;
    double cubicMiles = gallons*CUBIC_MILES_PER_GALLON;
    
    //prints the output
    System.out.println("" + cubicMiles + " cubic miles");
    
  } //ends the main methodn
  
} //ends the class