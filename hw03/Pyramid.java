///////////////////////
//Benjamin Aulenbach 9/18/18
//Pyramid.java
//CSE 002, BHA222
//determines the volume of a pyramid

import java.util.Scanner; //imports scanner object

public class Pyramid{ //creates a class needed to run the program

  public static void main(String[] args){ //sets up main method needed to run the program

    Scanner input = new Scanner(System.in); //creates new scanner object
    
    //asks user for input, length of square pyramid
    System.out.print("The square side of the pyramid is (input length): ");
    double side = input.nextDouble();
    
    //asks user for input, height of pyramid
    System.out.print("The height of the pyramid is (input height): ");
    double height = input.nextDouble();
    
    //calculates the volume and prints it to the screen
    double volume = side*side*height/3;
    System.out.println("The volume inside the pyramid is: " + volume);
    
  } //ends the main method

} //ends the class