////////////////////////////////////////////////////////////////////////
// Benjamin Aulenbach 9/25/18                                         //
// CrapsIf.java                                                       //
// CSE 002, BHA222                                                    //
// Determines what the name of the craps roll is using if statements  //
////////////////////////////////////////////////////////////////////////

import java.util.Scanner; //imports necessary tools for getting user input
import java.util.Random; //imports necessary tools for creating random data

public class CrapsIf{ //creates the main class needed to run the program

  public static void main(String[] args){ //creates the main method needed to run the program
    
    int dice1; int dice2; //initializes variables to keep them in scope of whole program
    
    Scanner input = new Scanner(System.in); //creates an instance of the scanner used to input data from the user
    Random rand = new Random(); //creates an instance of random used to create random integers
    
    //gets choice from user if they want to manually enter data or not
    System.out.print("Welcome to the Craps program!\nEnter a '1' to randomly select two dice rolls.\nEnter a '2' to manually enter the values.\n\nEnter here: ");
    int choice = input.nextInt(); 
    
    if (choice==1){ //if the user wants random dice rolls
      
      //sets a random value from 1 to 6 for each die
      dice1 = rand.nextInt(6) + 1;
      dice2 = rand.nextInt(6) + 1;
      
      //prints the randomly selected values
      System.out.println("");
      System.out.println(dice1);
      System.out.println(dice2);
      
    }else { //if the user wants to enter the values for each die
      
      //asks the user for the value of each die
      System.out.print("Enter value of first die: ");
      dice1 = input.nextInt();
      System.out.print("Enter value of second die: ");
      dice2 = input.nextInt();
      
    } //ends else block
    
    System.out.println("");
    
    //this if else block determines what the name of the pair is
    if (dice1 == dice2){ //if the dice are equal, a pattern for naming exists
      
      if(dice1 == 1){ //snake eyes
        System.out.println("Snake eyes");
      }else if(dice1 == 2){ //hard four
        System.out.println("Hard four");
      }else if(dice1 == 3){ //hard six
        System.out.println("Hard six");
      }else if(dice1 == 4){ //hard eight
        System.out.println("Hard eight");
      }else if(dice1 == 5){ //hard ten
        System.out.println("Hard ten");
      }else { //boxcars
        System.out.println("Boxcars");
      }
      
    }else if (dice1 + dice2 == 3){ //ace deuce
    
       System.out.println("Ace deuce");
      
    }else if (dice1 + dice2 == 4){ //easy four
    
       System.out.println("Easy four");
      
    }else if (dice1 + dice2 == 5){ //fever five
    
       System.out.println("Fever five");
      
    }else if (dice1 + dice2 == 6){ //easy six
    
       System.out.println("Easy six");
      
    }else if (dice1 + dice2 == 7){ //seven out
    
       System.out.println("Seven out");
      
    }else if (dice1 + dice2 == 8){ //easy eight
    
       System.out.println("Easy eight");
      
    }else if (dice1 + dice2 == 9){ //nine
    
       System.out.println("Nine");
      
    }else if (dice1 + dice2 == 10){ //easy ten
    
       System.out.println("Easy ten");
      
    }else if (dice1 + dice2 == 11){ //yo-leven
    
       System.out.println("Yo-leven");
      
    }else{
      
      System.out.println("Programmer error.");
      
    } //ends if else block
    
  } //ends the main mehtod

} //ends the class method