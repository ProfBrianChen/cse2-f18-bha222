////////////////////////////////////////////////////////////////////////////
// Benjamin Aulenbach 9/25/18                                             //
// CrapsSwitch.java                                                       //
// CSE 002, BHA222                                                        //
// Determines what the name of the craps roll is using switch statements  //
////////////////////////////////////////////////////////////////////////////

import java.util.Scanner; //imports necessary tools for getting user input
import java.util.Random; //imports necessary tools for creating random data

public class CrapsSwitch{ //creates the main class needed to run the program

  public static void main(String[] args){ //creates the main method needed to run the program
  
    int dice1 = 0; int dice2 = 0; String ans = ""; //initializes variables to keep them in scope of whole program
    
    Scanner input = new Scanner(System.in); //creates an instance of the scanner used to input data from the user
    Random rand = new Random(); //creates an instance of random used to create random integers
    
    //gets choice from user if they want to manually enter data or not
    System.out.print("Welcome to the Craps program!\nEnter a '1' to randomly select two dice rolls.\nEnter a '2' to manually enter the values.\n\nEnter here: ");
    int choice = input.nextInt();
    
    switch (choice){
      case 1:
        
        //sets a random value from 1 to 6 for each die
        dice1 = rand.nextInt(6) + 1;
        dice2 = rand.nextInt(6) + 1;
      
        //prints the randomly selected values
        System.out.println("");
        System.out.println(dice1);
        System.out.println(dice2);
        break;
        
      case 2:
        
        //asks the user for the value of each die
        System.out.print("Enter value of first die: ");
        dice1 = input.nextInt();
        System.out.print("Enter value of second die: ");
        dice2 = input.nextInt();
        break;
        
    } //ends the switch block
    
    System.out.println("");
    
    switch (dice1){
        
      case 1: //if die 1 == 1
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Snake eyes";
            break;
          case 2: //if die 2 == 2
            ans = "Ace deuce";
            break;
          case 3: //if die 2 == 3
            ans = "Easy four";
            break;
          case 4: //if die 2 == 4
            ans = "Fever five";
            break;
          case 5: //if die 2 == 5
            ans = "Easy six";
            break;
          case 6: //if die 2 == 6
            ans = "Seven out";
            break;
        }
        break;
        
      case 2: //if die 1 == 2
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Ace deuce";
            break;
          case 2: //if die 2 == 2
            ans = "Hard four";
            break;
          case 3: //if die 2 == 3
            ans = "Fever five";
            break;
          case 4: //if die 2 == 4
            ans = "Easy six";
            break;
          case 5: //if die 2 == 5
            ans = "Seven out";
            break;
          case 6: //if die 2 == 6
            ans = "Easy eight";
            break;
        }
        break;
        
      case 3: //if die 1 == 3
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Easy four";
            break;
          case 2: //if die 2 == 2
            ans = "Fever five";
            break;
          case 3: //if die 2 == 3
            ans = "Hard six";
            break;
          case 4: //if die 2 == 4
            ans = "Seven out";
            break;
          case 5: //if die 2 == 5
            ans = "Easy eight";
            break;
          case 6: //if die 2 == 6
            ans = "Nine";
            break;
        }
        break;
      
      case 4: //if die 1 == 4
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Fever five";
            break;
          case 2: //if die 2 == 2
            ans = "Easy six";
            break;
          case 3: //if die 2 == 3
            ans = "Seven out";
            break;
          case 4: //if die 2 == 4
            ans = "Hard eight";
            break;
          case 5: //if die 2 == 5
            ans = "Nine";
            break;
          case 6: //if die 2 == 6
            ans = "Easy ten";
            break;
        }
        break;
        
      case 5: //if die 1 == 5
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Easy six";
            break;
          case 2: //if die 2 == 2
            ans = "Seven out";
            break;
          case 3: //if die 2 == 3
            ans = "Easy eight";
            break;
          case 4: //if die 2 == 4
            ans = "Nine";
            break;
          case 5: //if die 2 == 5
            ans = "Hard ten";
            break;
          case 6: //if die 2 == 6
            ans = "Yo-leven";
            break;
        }
        break;
        
      case 6: //if die 1 == 6
        switch (dice2){ 
          case 1: //if die 2 == 1
            ans = "Seven out";
            break;
          case 2: //if die 2 == 2
            ans = "Easy eight";
            break;
          case 3: //if die 2 == 3
            ans = "Nine";
            break;
          case 4: //if die 2 == 4
            ans = "Easy ten";
            break;
          case 5: //if die 2 == 5
            ans = "Yo-leven";
            break;
          case 6: //if die 2 == 6
            ans = "Boxcars";
            break;
        }
        break;
        
    } //ends the switch block
    
    System.out.println(ans);
    
  } //ends the main method
  
} //ends the class