////////////////////////////////////////////////////////////////////////
// Benjamin Aulenbach 10/9/18                                         //
// hw05.java                                                          //
// CSE 002, BHA222                                                    //
// Determines the probability of a hand of card in poker              //
////////////////////////////////////////////////////////////////////////

import java.util.Scanner; //imports scanner class needed for input
import java.util.Random;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class hw05 { //creates class needed to run program
	
	public static void main(String[] args) { //creates main method needed to run program
		
		//initializes variables
		int numPairs = 0;
		int num2Pairs = 0;
		int num3Kind = 0;
		int num4Kind = 0;
		int numHands = 0;
		int x1=0, x2=0, x3=0, x4=0, x5=0;

		Scanner s = new Scanner(System.in); //creates new scanner object
		Random r = new Random(); //creates new random object
		
		System.out.print("Enter number of hands: "); //prompts the user for input
		numHands = s.nextInt(); //gets input from user
		
		for(int i = 0; i<numHands; i++){ //for loop to create hands and check what category they fall in
			
			x1 = r.nextInt(52) + 1; //creates a random number (card) in range 1,52
			
			do{ //loops while x1 = x2, still does one case no matter what to initialize x2
				x2 = r.nextInt(52) + 1;  //creates a random number (card) in range 1,52
			}while (x1 == x2);
			
			do{ //loops until unique card is pulled
				x3 = r.nextInt(52) + 1; //creates a random number (card) in range 1,52
			}while (x3==x2 || x3==x1);
			
			do{
				x4 = r.nextInt(52) + 1;
			}while (x4==x3 || x4==x2 || x4==x1);
			
			do{
				x5 = r.nextInt(52) + 1;
			}while (x5==x4 || x5==x3 || x5==x2 || x5==x1);
		
		
			//take mod
			x1 %= 13;
			x2 %= 13;
			x3 %= 13;
			x4 %= 13;
			x5 %= 13;
			
			//creates an array to sort numbers
			int[] intArray = new int[5];
			intArray[0] = x1;
			intArray[1] = x2;
			intArray[2] = x3;
			intArray[3] = x4;
			intArray[4] = x5;
			
			Arrays.sort(intArray);
			
			//see if there are pairs, 3 of kinds, and 4 of kinds
			if(intArray[0] == intArray[3] || intArray[1] == intArray[4]) {
				num4Kind += 1;
			}else if(intArray[0] == intArray[2] || intArray[1] == intArray[3] || intArray[2] == intArray[4]) {
				num3Kind += 1;
			}else if((intArray[0] == intArray[1] && intArray[2] == intArray[3]) || (intArray[1] == intArray[2] && intArray[3] == intArray[4])) {
				num2Pairs += 1;
			}else if((intArray[0] == intArray[1]) || (intArray[1] == intArray[2]) || (intArray[2] == intArray[3]) || (intArray[3] == intArray[4])) {
				numPairs += 1;
			}

		}
		
		//prints data
		
		NumberFormat formatter = new DecimalFormat("#0.000");
		
		System.out.println("The number of loops: " + numHands);
		System.out.println("The probability of Four-of-a-kind: " + formatter.format((double)num4Kind/numHands));
		System.out.println("The probability of Three-of-a-kind: " + formatter.format((double)num3Kind/numHands));
		System.out.println("The probability of Two-pair: " + formatter.format((double)num2Pairs/numHands));
		System.out.println("The probability of One-pair: " + formatter.format((double)numPairs/numHands));
		
	} //ends main method

} //ends class method