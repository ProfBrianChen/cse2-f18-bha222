////////////////////////////////////
//Benjamin Aulenbach 10/23/18    //
//EncryptedX.java                //
//CSE 002, BHA222                //
//Hides an X in a box of stars   //
////////////////////////////////////

import java.util.Scanner; //imports the scanner class

public class EncryptedX { //creates class needed to run the program

	public static void main(String[] args) { //creates main method needed to run program
		
		//initializes variables
		boolean check = false;
		int num = 0;
		String x = "";
		Scanner s = new Scanner(System.in);
		
		do { //use a do while loop to check for valid input
			
			System.out.print("Enter number for size of box (0-100): "); //prompts user for input
			check = s.hasNextInt(); //checks if input is an integer
			
			if(check) { //if the user actually entered an integer
				
				num = s.nextInt(); //collects the input
				
				if (num>100 || num<0) { //if the numbers are outside the range
					System.out.println("Input out of range, try again."); //tells the user they messed up
					check = false; //have the user retry
				} //ends if statement
				
			} //ends if statement
			
			else { //if the user entered a non-integer
				
				System.out.println("Invalid input, try again."); //tells the user they messed up
				x = s.next(); //collects bad input
				
			} //ends if statement
			
		}while (!check); //ends do while loop and continues the loop if 'check' is false
		
		for(int i=1; i<=num+1; i++) { //starts a loop in range of what user inputted
			
			for(int j=1; j<=num+1; j++) { //starts a nested loop
				
				if (i==j || j==(num-i+2)) { //determines if it should be a space
					
					System.out.print(" "); //prints a space
					
				} //closes the if
				
				else { //if there should be a star
					
					System.out.print("*"); //prints the star
					
				} //closes the else
				
			} //ends nested loop
			
			System.out.println(""); //goes to next line for box
			
		} //ends main loop
		
	} //ends main method
	
} //ends class
