///////////////////////////////////////
// Benjamin Aulenbach 10/31/18       //
// hw07.java                         //
// CSE 002, BHA222                   //
// Utilizes methods to perform tasks //
///////////////////////////////////////

import java.util.Scanner; //imports the scanner class

public class hw07 { //creates class needed to run java program

	public static void main(String[] args) { //creates main method needed for java program
		
		Scanner s = new Scanner(System.in); //creates a new instance of the scanner class
		
		String text = sampleText(s); //calls the sampleText method
		printMenu(text); //calls the printMenu method
		
	} //ends main method
	
	public static String sampleText(Scanner s) { //creates sampleText method
		
		System.out.print("Enter a sample text: "); //prompts the user to enter a string
		String text = s.nextLine(); //collects the text from the user
		
		System.out.println("You entered: " + text); //prints the output
		
		return text;
		
	} //ends sampleText method
	
	public static void printMenu(String text) { //creates printMenu method
		
		Scanner s = new Scanner(System.in); //creates a new instance of scanner
		
		System.out.print("\nMENU\n\nc: Number of non-whitespace characters\nw: Number of words\nf: Find text\nr: Replace all !'s\ns: Shorten spaces\nq: Quit\n\nChoose an option: "); //prints the options and prompts the user for input
		
		String choice = s.nextLine(); //collects input from user
		
		if (choice.equals("c")) { //non-whitespace characters
			getNumOfNonWSCharacters(text); //calls required method
		} //ends if
		else if (choice.equals("w")) { //number of words
			getNumOfWords(text); //calls required method
		} //ends else if
		else if (choice.equals("f")) { //find text
			findText(text); //calls required method
		} //ends else if
		else if (choice.equals("r")) { //replace
			text = replaceExclamation(text); //calls required method
			System.out.println("Edited text: " + text); //prints the output
			printMenu(text); //go back to the menu
		} //ends else if
		else if (choice.equals("s")) { //shorten
			text = shortenSpace(text); //calls required method
			System.out.println("Edited text: " + text); //prints the output
			printMenu(text); //go back to the menu
		} //ends else if
		else if (choice.equals("q")) { //quit
			System.out.println("\nThank you for using this program.\nEnd.");
		} //ends else if
		else { //invalid
			System.out.println("\nInvalid input, please try again."); //tells user they messed up
			printMenu(text); //goes back through the printMenu function
		} //ends else
		
	} //ends printMenu method
	
	public static void getNumOfNonWSCharacters(String text) { //creates getNumOfNonWSCharacters method
		
		int count = 0; //initializes count variable
		
		for (int i=0; i<text.length(); i++) { //go through all characters
			if (text.charAt(i) != 32) { //if at that char it's a space
				count += 1; //adds one to the counter
			} //ends if
		} //ends loop
		
		System.out.println("\nNumber of non-whitespace characters: " + count); //prints output
		
		printMenu(text); //go back to the menu
		
	} //ends getNumOfNonWSCharacters method
	
	public static void getNumOfWords(String text) { //creates getNumOfWords method
		
		int count = 0; //initializes count variable
		
		for (int i=1; i<text.length();i++) { //go through all characters
			if(text.charAt(i) == 32 && text.charAt(i-1) != 32) { //if it's a space
				count += 1; //increase word count by one
			} //ends if
		} //ends loop
		
		count += 1; //to get last word
		
		System.out.println("\nNumber of words: " + count); //prints the output
		
		printMenu(text); //go back to the menu
		
	} //ends getNumOfWords method
	
	public static void findText(String text) { //creates findText method
		
		Scanner s = new Scanner(System.in); //creates a new scanner object
		
		int count = 0; //initializes counter
		
		System.out.print("Enter a word or phrase to be found: "); //prompts the user for input
		String subText = s.nextLine(); //collects the input
		
		int subLength = subText.length(); //gets length of the word(s) that is being searched for
		
		for (int i=0; i<text.length()-subLength;i++) { //go through the possible starting points of searched word
			if(text.substring(i,i+subLength).equals(subText)) { //if the word is in the phrase
				count += 1; //add one to the counter
			} //end if
		} //end loop
		
		System.out.println("\"" + subText + "\" instances: " + count); //prints the output
		
		printMenu(text); //go back to the menu
		
	} //ends findText method
	
	public static String replaceExclamation(String text) { //creates replaceExclamation method
		
		String newText = "";
		
		for(int i=0; i<text.length(); i++) { //loops through all characters in text string
			if(text.charAt(i) == 33) { //if it's an exclamation point
				newText += '.'; //replace with period
			} //end if
			else { //otherwiser
				newText += text.charAt(i);
			}
		} //end loop
		
		return newText;
		
	} //ends replaceExclamation method
	
	public static String shortenSpace(String text) { //creates shortenSpace method
		
		boolean check = false; //initializes a checker
		String newText = ""; //initializes new text variable
		
		for(int i=0; i<text.length(); i++) { //goes through all characters
			if(text.charAt(i) == 32 && !check) { //if it's a space
				check = true; //there has now been one space
				newText += " "; //adds the single space
			} //ends if
			else if(text.charAt(i) != 32) { //if not a space
				check = false; //now a doubl/triple/... space has not happened
				newText += text.charAt(i); //add to new Text
			} //ends else if
		} //ends loop
		
		return newText;
		
	} //ends shortenSpace method
	
} //ends class