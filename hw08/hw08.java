////////////////////////////////////////
// hw08.java                          //
// CSE 002, BHA222                    //
// Practice utilizing arrays          //
////////////////////////////////////////

import java.util.Scanner; //imports Scanner class
import java.util.Random; //imports Random class

public class hw08 { //creates class needed to run java program
	
	public static void main(String[] args) {//creates main method needed to run java program
		
		//initializes variables
		int numCards = 0;
		int again = 1;
		int index = 51;
		
		//starter code given in hw spec
		Scanner scan = new Scanner(System.in); //creates new instance of scanner class
		
		//suits club, heart, spade, or diamond
		String[] suitNames = {"C", "H", "S", "D"};
		String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
		String[] cards = new String[52];

		//prompts user for input
		System.out.print("Enter the length of the hand: ");
		numCards = scan.nextInt();
		
		//creates array of number of cards
		String[] hand = new String[numCards];
		
		//creates array of deck of cards
		for(int i=0; i<52; i++) {
			cards[i] = rankNames[i%13]+suitNames[i/13];
		}
		
		//Calls methods to shuffle and print arrays
		System.out.println();
		printArray(cards);
		shuffle(cards);
		System.out.println("Shuffled");
		printArray(cards);
		
		//while the user wants a new hand, create a new hand
		while(again == 1) {
			hand = getHand(cards, index, numCards);
			if (hand == null) { //if there weren't enough cards, make new deck
				for(int i=0; i<52; i++) {
					cards[i] = rankNames[i%13]+suitNames[i/13];
					System.out.print(cards[i]+" ");
				}
				System.out.println();
				shuffle(cards);
				System.out.println("Shuffled");
				index = 51;
			}
			else { //else print out hand and ask if they want a new hand
				System.out.println("Hand");
				printArray(hand);
				index = index-numCards;
				System.out.print("Enter a 1 if you want another hand drawm: ");
				again = scan.nextInt();
				System.out.println();
			}
		}
		
	} //ends main method
	
	//Shuffles the elements in the array
	public static void shuffle(String[] list) {
		
		Random rand = new Random(); //creates instance of random class
		
		//mixes up the array
		for(int i=0; i<100; i++) {
			int n = rand.nextInt(52);
			String temp = list[0];
			list[0] = list[n];
			list[n] = temp;
		}
		
		//don't have to return because 'list' points to 'cards'
		//therefore it changes in the main method as well
		
	} //ends shuffle method
	
	//creates a hand of cards from the shuffled deck
	public static String[] getHand(String[] list, int index, int numCards) {
		
		//if there are enough cards left in the deck
		if (index+1 >= numCards) {
			String[] hand = new String[numCards];
			for(int i=0; i<numCards; i++) {
				hand[i] = list[index-i];
			}
			return hand;
		}
		//else return null to prompt creation of new deck
		else {
			return null;
		}
	
		
	} //ends getHand method
	
	
	//Prints the array given
	public static void printArray(String[] list) {
		
		for(int i=0;i<list.length;i++) {
			System.out.print("" +list[i] + " ");
		}
		System.out.println();
		
	} //ends printArray method

} //ends class