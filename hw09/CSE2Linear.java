////////////////////////////////////////
// Benjamin Aulenbach                 //
// CSE2Linear                         //
// CSE 002, BHA222                    //
// Practice searching arrays          //
////////////////////////////////////////

import java.util.Scanner; //imports the scanner class
import java.util.Random; // imports the random class

public class CSE2Linear { // creates class needed to run java program

	public static void main(String[] args) { // creates main method needed to run java program

		// initialize variables
		Scanner s = new Scanner(System.in); // creates an instance of the scanner class
		int lowGrade = 0; // creates new variable "lowGrade" and initializes the value to 0
		int[] grades = new int[15]; // initializes grades array
		int searchFor = 0;
		boolean check = false;
		String x = null;

		System.out.println("Enter 15 ascending ints for final grades in CSE2"); // tells the user what they will be doing

		for (int i = 0; i < 15; i++) { // loops for 15 grades

			check = false; // reset checker

			do { // loop at least once

				System.out.print("Enter a number greater than " + (lowGrade - 1) + ": "); // prompts the user for input
				check = s.hasNextInt(); // checks to see if the input is an integer

				if (!check) { // if the user didn't enter an integer
					System.out.println("Please enter only an integer."); // tells user they messed up
					x = s.nextLine(); // collects garbage input
				} // ends if

				else { // if the user actually entered an integer
					int numb = s.nextInt(); // collect input
					if (numb > -1 && numb < 101) { // if number is in range
						if (numb >= lowGrade) { // if number is incremental
							lowGrade = numb; // update lowGrade's value\
							grades[i] = lowGrade; // enter value into array
						} // end if
						else { // if number is not incremental
							System.out.println("Please enter an integer greater than " + (lowGrade - 1) + "."); // tells user they messed up
							check = false; // loop again
						} // end else
					} // ends if
					else { // if number is not in range
						System.out.println("Please enter an integer in range (0-100) and greater than " + (lowGrade - 1) + "."); // tells user they messed up
						check = false; // loop again
					} // ends else
					x = s.nextLine(); // collect rest of input
				} // ends else

			} while (check == false); // loop until user enters valid integer

		} // ends for loop

		System.out.println(); // go to next line

		for (int i = 0; i < 15; i++) { // loops through every element to print it
			System.out.print("" + grades[i] + " ");
		} // ends for loop

		System.out.println(); // go to next line
		
		do { // loop at least once

			System.out.print("Enter a grade to be searched for: "); // prompts the user for input
			check = s.hasNextInt(); // checks to see if the input is an integer

			if (!check) { // if the user didn't enter an integer
				System.out.println("Please enter only an integer."); // tells user they messed up
				x = s.nextLine(); // collects garbage input
			} // ends if

			else { // if the user actually entered an integer
				int numb = s.nextInt(); // collect input
				if (numb > -1 && numb < 101) { // if number is in range
					searchFor = numb;
				} // ends if
				else { // if number is not in range
					System.out.println("Please enter an integer in range (0-100)."); // tells user they messed up
					check = false; // loop again
				} // ends else
				x = s.nextLine(); // collect rest of input
			} // ends else

		} while (check == false); // loop until user enters valid integer
		
		//calls methods
		binarySearch(grades, searchFor);
		scramble(grades);
		
		System.out.println("Scrambled");
		
		for (int i = 0; i < 15; i++) { // loops through every element to print it
			System.out.print("" + grades[i] + " ");
		} // ends for loop
		
		System.out.println();
		
		do { // loop at least once

			System.out.print("Enter a grade to be searched for: "); // prompts the user for input
			check = s.hasNextInt(); // checks to see if the input is an integer

			if (!check) { // if the user didn't enter an integer
				System.out.println("Please enter only an integer."); // tells user they messed up
				x = s.nextLine(); // collects garbage input
			} // ends if

			else { // if the user actually entered an integer
				int numb = s.nextInt(); // collect input
				if (numb > -1 && numb < 101) { // if number is in range
					searchFor = numb;
				} // ends if
				else { // if number is not in range
					System.out.println("Please enter an integer in range (0-100)."); // tells user they messed up
					check = false; // loop again
				} // ends else
				x = s.nextLine(); // collect rest of input
			} // ends else

		} while (check == false); // loop until user enters valid integer
		
		linearSearch(grades, searchFor); //calls linearSearch method
	} // ends main method

	public static void binarySearch(int list[], int number) { //initializes binarySearch method

		//initializes variables
		int beg = 0;
		int end = list.length-1;
		int mid = 0;
		int count = 0;
		boolean check = true;
		
		while (check) { //while the element has not been found
			
			count++;
			mid = (end+beg)/2; //find midpoint
			
			if (list[mid] == number)  { //if the number was found
				System.out.println("" + number + " was found with " + count + " iterations."); //tels user it was found
				check = false; //do not need to keep looping
			} //ends if statement
			else if (list[mid] > number) { //if list element is greater than what is being searched
				end = mid-1;
			} //ends else if
			else if (list[mid] < number) { //if list element is less than what is being searched
				beg = mid+1;
			} //ends else if
			
			if (end-beg < 0) { //if the element was not found
				System.out.println("" + number + " was not found with " + count + " iterations.");
				check = false;
			}
			
		} //ends while loop
		
	} //ends binarySearch method
	
	public static void scramble(int[] list) { //initializes scramble method

		Random rand = new Random(); //creates instance of random class
		
		//mixes up the array
		for(int i=0; i<100; i++) {
			int n = rand.nextInt(15);
			int temp = list[0];
			list[0] = list[n];
			list[n] = temp;
		}
		
		//don't have to return because 'list' points to 'grades'
		//therefore it changes in the main method as well
		
	} //ends scramble method
	
	public static void linearSearch(int[] list, int number) { //initializes linearSearch method
		
		boolean check = false; //checks if the number was found
		
		for (int i=0; i<list.length; i++) {
			if(list[i] == number) {
				System.out.println("" + number + " was found with " + (i+1) + " iterations.");
				check = true;
			}
		}
		
		if(!check) { //if the element was not found
			System.out.println("" + number + " was not found with " + (list.length) + " iterations.");
		} //ends if statement
		
	} //ends linearSearch method

} // ends class