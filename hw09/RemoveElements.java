//////////////////////////////////////////////////
// Benjamin Aulenbach                           //
// RemoveElements                               //
// CSE 002, BHA222                              //
// Practice deleting items from arrays          //
//////////////////////////////////////////////////

import java.util.Scanner; //imports scanner class
import java.util.Random;

public class RemoveElements { //initializes class

	//code given on hw spec
	public static void main(String[] args) { //initializes main method
		
		Scanner scan = new Scanner(System.in);
		int num[] = new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer = "";
		do {
			System.out.println("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The orignial array is: ";
			out += listArray(num);
			System.out.println(out);
			
			System.out.print("Enter the index: ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1 = "The output array is: ";
			out1 += listArray(newArray1); //return a string of the form "{2,3,-9}"
			System.out.println(out1);
			
			System.out.print("Enter the target value: ");
			target = scan.nextInt();
			newArray2 = remove(num,target);
			String out2 = "The output array is: ";
			out2 += listArray(newArray2); //return a string of the form "{2,3,-9}"
			System.out.println(out2);;
			
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit: ");
			answer = scan.next();
		} while (answer.equals("Y") || answer.equals("y"));
		
	} //ends main method
	
	//code given on hw spec
	public static String listArray(int num[]) {
		String out = "{";
		for (int j=0; j<num.length; j++) {
			if (j>0) {
				out += ", ";
			}
			out += num[j];
		}
		out += "} ";
		return out;
	}
	
	public static int[] randomInput() { //creates randomInput method
		
		//initialize variables
		Random rand = new Random();
		int[] list = new int[10];
		
		for (int i=0; i<list.length; i++) { //go through every item in list
			list[i] = rand.nextInt(10); //generate random number and put in list
		} //ends for loop
		
		return list; //return the list
		
	}
	
	public static int[] delete(int[] list, int pos) {
		
		Scanner scan = new Scanner(System.in);
		
		if (pos > 9 || pos < 0) {
			System.out.println("The index is not valid.");
			return list;
		}
		else {
			//initialize variables
			int[] list2 = new int[list.length-1];
			boolean check = true;
		
			for (int i=0; i<list.length; i++) { //go through every item in list
				if (i != pos && check) { //if it is not the skipped position
					list2[i] = list[i];
				} //ends if
				else if (i != pos) { //if it is not the skipped position and skipped position already occured
					list2[i-1] = list[i];
				} //ends else if
				else {
					check = false;
				}
			} //ends for loop
		
			return list2;
		}
	}
	
	public static int[] remove(int[] list, int target) {
		
		int count = 0;
		
		//count number of times target is in array
		for (int i=0; i<list.length; i++) {
			if (list[i] != target) {
				count++;
			}
		}
		
		if (count != list.length) {
			System.out.println("Element " + target + " has been found.");
		}
		else {
			System.out.println("Element " + target + " has not been found.");
		}
		
		//create new array
		int[] list2 = new int[count];
		count = 0;
		
		//loops through and enters all non-targets
		for (int i=0; i<list.length; i++) {
			if (list[i] != target) {
				list2[i-count] = list[i];
			}
			else {
				count++;
			}
		}
		
		return list2;
		
	}
	
} //ends class