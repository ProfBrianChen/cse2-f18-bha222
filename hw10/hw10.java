///////////////////////////////////////
// Benjamin Aulenbach 12/04/18       //
// hw10.java                         //
// CSE 002, BHA222                   //
// Play a game of tic-tac-toe        //
///////////////////////////////////////

import java.util.Scanner; //imports the scanner class

public class hw10 { // initializes hw10 class

	public static void main(String args[]) { // initializes main method

		// initialize variables
		char[][] board = {{'1','2','3'},{'4','5','6'},{'7','8','9'}};
		int counter = board.length * board[0].length;
		boolean check = true; // determines whose turn it is
		boolean check4win = false; //determines if the game has been won

		while (counter > 0 && !check4win) { // keep looping based on places left on board

			print(board); // print the board

			if (check) { // if it is player 1's turn
				input(board, 1);
			} // end if
			else {;
				input(board, 2);
			}

			counter--; // decrease counter
			
			if (check) { // if it is player 1's turn
				check4win = winCheck(board, 1);
			} // end if
			else {
				check4win = winCheck(board, 2);
			}
			
			if (!check4win) {
				drawCheck(counter);
			}
			
			check = !check; // change whose turn it is

		} // ends while loop

	} // ends main method

	public static void print(char[][] board) { // initializes print method

		for (int i = 0; i < board.length; i++) { // loops through rows
			for (int j = 0; j < board[0].length; j++) { // loops through columns
				System.out.print("" + board[i][j] + "\t");
			} // ends for loop
			System.out.println(); // go to next line
		} // ends for loop

	} // ends print method

	public static void input(char[][] board, int player) { //initializes input method
		
		Scanner s = new Scanner(System.in) ;
		boolean doCheck = false;
		
		do {
			
			if (player == 1) {
				System.out.print("Player 1, enter your position: ");
			}
			else {
				System.out.print("Player 2, enter your position: ");
			}
		
			boolean check = s.hasNextInt(); //check if the user actually entered an integer
			
			if (check) { //if it was an integer
				int num = s.nextInt(); //collects input
				if (num%3 != 0) {
					if(num > 0 && num < 10) {
						if(board[(num-1)/3][(num%3-1)] != 'X' && board[(num-1)/3][(num%3-1)] != 'O') {
							if (player == 1) {
								board[(num-1)/3][(num%3-1)] = 'O';
							}
							else {
								board[(num-1)/3][(num%3-1)] = 'X';
							}
							doCheck = true;
						}
						else {
							System.out.println("Error: Position already taken.");
						}
						
					}
					else {
						System.out.println("Error: Enter an open position between (1-9).");
					}
				}
				else {
					if(num > 0 && num < 10) {
						if(board[(num-1)/3][2] != 'X' && board[(num-1)/3][2] != 'O') {
							if (player == 1) {
								board[(num-1)/3][2] = 'O';
							}
							else {
								board[(num-1)/3][2] = 'X';
							}
							doCheck = true;
						}
						else {
							System.out.println("Error: Position already taken.");
						}
					}
					else {
						System.out.println("Error: Enter an open position between (1-9).");
					}
				}
			} //ends if 
			
			else { //if it was a string
				System.out.println("Error: Please enter an integer.");
				String x = s.nextLine();
			} //ends else
			
		} while (!doCheck);
		
	} // ends input method
	
	//checks if the game has been won
	public static boolean winCheck(char[][] board, int player) {
		
		boolean answer = false;
		
		if (player == 1) {
			
			//if there are three in a row
			if(board[0][0]=='O' && board[0][1]=='O' && board[0][2]=='O') {
				answer = true;
			}
			else if (board[1][0]=='O' && board[1][1]=='O' && board[1][2]=='O') {
				answer = true;
			}
			else if (board[2][0]=='O' && board[2][1]=='O' && board[2][2]=='O') {
				answer = true;
			}
			else if (board[0][0]=='O' && board[1][0]=='O' && board[2][0]=='O') {
				answer = true;
			}
			else if (board[0][1]=='O' && board[1][1]=='O' && board[2][1]=='O') {
				answer = true;
			}
			else if (board[0][2]=='O' && board[1][2]=='O' && board[2][2]=='O') {
				answer = true;
			}
			else if (board[0][0]=='O' && board[1][1]=='O' && board[2][2]=='O') {
				answer = true;
			}
			else if (board[0][2]=='O' && board[1][1]=='O' && board[2][0]=='O') {
				answer = true;
			}
			
			if (answer) {
				print(board);
				System.out.println("Congratulations, Player 1 wins!");
			}
		}
		
		else {
			
			//if there are three in a row
			if(board[0][0]=='X' && board[0][1]=='X' && board[0][2]=='X') {
				answer = true;
			}
			else if (board[1][0]=='X' && board[1][1]=='X' && board[1][2]=='X') {
				answer = true;
			}
			else if (board[2][0]=='X' && board[2][1]=='X' && board[2][2]=='X') {
				answer = true;
			}
			else if (board[0][0]=='X' && board[1][0]=='X' && board[2][0]=='X') {
				answer = true;
			}
			else if (board[0][1]=='X' && board[1][1]=='X' && board[2][1]=='X') {
				answer = true;
			}
			else if (board[0][2]=='X' && board[1][2]=='X' && board[2][2]=='X') {
				answer = true;
			}
			else if (board[0][0]=='X' && board[1][1]=='X' && board[2][2]=='X') {
				answer = true;
			}
			else if (board[0][2]=='X' && board[1][1]=='X' && board[2][0]=='X') {
				answer = true;
			}
			
			if (answer) {
				print(board);
				System.out.println("Congratulations, Player 2 wins!");
			}
			
		}
		
		return answer;
	}
	
	public static void drawCheck(int num) {
		if (num < 1) {
			System.out.println("The game ended in a draw.");
		}
	}

} // ends hw10 class