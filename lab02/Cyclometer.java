///////////////////////
//Benjamin Aulenbach 9/7/18
//Cyclometer.java
//Prints information on bike trips

public class Cyclometer {
  
  //main method required for every Java program
  public static void main(String[] arsg){
    
    //Our input data
    int secsTrip1 = 480;  //time of trip #1 in seconds
    int secsTrip2 = 3220;  //time of trip #2 in seconds
		int countsTrip1 = 1561;  //number of times the front wheel completed one rotation in the first trip
		int countsTrip2 = 9037; //number of times the front wheel completed one rotation in the second trip
    
    //Intermediate variables and output data
    double wheelDiameter = 27.0,  //the diameter of the wheel, used to find circumference of the wheel. In inches
  	PI = 3.14159, //the value of pi, used to find the circumference of the wheel
  	feetPerMile = 5280,  //number of feet in a mile, used to determine how many miles traveled from feet traveled
  	inchesPerFoot = 12,   //number of inches in a foot, used to determine how many feet traveled from inches
  	secondsPerMinute = 60;  //number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //initializes variables for total distance for each trip and the trips combined
    
    //printing the first information for the trips (time and counts)
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); 
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //does the same calculation for distanceTrip2 that was done for distanceTrip1
	  totalDistance = distanceTrip1 + distanceTrip2; //adds distances to find total distance
    
    //prints the second information for the trips (distance and total distance)
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	  System.out.println("The total distance was " + totalDistance + " miles");

    
  } //end of main mehtod

} //end of class