/////////////////////////////
// BENJAMIN AULENBACH, BHA222
// CSE2, LAB03
// Splits a check among friends

import java.util.Scanner; //allows input from user

public class Check{ //initiates the class
  
  public static void main(String args[]){ //main method required for program
    
    Scanner myScanner = new Scanner( System.in ); //declares instance of scanner
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt for the user to input a check amount
    double checkCost = myScanner.nextDouble(); //gets the value of the check
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompts the user for a percentage tip to pay
    double tipPercent = myScanner.nextDouble(); //gets the value of the tip
    tipPercent /= 100; //converts the percentage to a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompts the user to input number of people
    int numPeople = myScanner.nextInt(); //gets the value of number of people
    
    double totalCost; //initiates value for totalCost
    double costPerPerson; //initiates value for cost of each person
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits to right of decimal point

    totalCost = checkCost * (1 + tipPercent); //uses tip to find total cost
    costPerPerson = totalCost / numPeople; //divides the total cost among all the people
    dollars=(int)costPerPerson; //this is just the dollar amount the people need to pay
    dimes=(int)(costPerPerson * 10) % 10; //gets tenth place of cents owed
    pennies=(int)(costPerPerson * 100) % 10; //gets hundreth place of pennies owed
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //prints output

  } //ends main method
  
} //ends class