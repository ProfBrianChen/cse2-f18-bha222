/////////////////////////////
// BENJAMIN AULENBACH, BHA222
// CSE2, LAB04
// CardGenerator.java
// Creates a deck of cards and randomly selects one

import java.util.Random; //imports the random object used later in program

public class CardGenerator{ //initiates the class
  
  public static void main(String[] args){ //creates the main method
  
    //initializes variables
    String num = "";
    String suit = "";
    
    Random randGen = new Random(); //creates an instance of Random
    
    int x = randGen.nextInt(52) + 1; //creates a random number from 1 to 52
    
    //checks if what suit number is in by using the specified ranges
    if (x <= 13){
      suit = "Diamonds";
    }
    else if (x<=26){
      suit = "Clubs";
    }
    else if (x<=39){
      suit = "Hearts";
    }
    else{
      suit = "Spades";
    }
    
    x = x%13; //takes the modulus of x to determine what value it has
    
    //uses the switch method to give a value to num based on the modulus value of x
    switch (x){
      case 1: num = "Ace";
        break;
      case 2: num = "Two";
        break;
      case 3: num = "Three";
        break;
      case 4: num = "Four";
        break;
      case 5: num = "Five";
        break;
      case 6: num = "Six";
        break;
      case 7: num = "Seven";
        break;
      case 8: num = "Eight";
        break;
      case 9: num = "Nine";
        break;
      case 10: num = "Ten";
        break;
      case 11: num = "Jack";
        break;
      case 12: num = "Queen";
        break;
      case 0: num = "King";
        break;
    }
    
    System.out.println("You picked the " + num + " of " + suit + "."); //prints out the output
  
  } //ends the main method
  
} //ernds the class