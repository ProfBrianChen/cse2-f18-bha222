import java.util.Scanner; //imports the scanner class necessary to get input

public class lab05{ //creates class needed to run the program
  
  public static void main(String[] args){ //creates main method needed to run the program
    
    //intializes variables
    boolean courseNumCheck;
    boolean meetingsCheck;
    boolean studentsCheck;
    boolean timeCheck;
    String x;
    
    Scanner s = new Scanner(System.in); //creates a new scanner instance to receive input from user
    
    do{
    
      System.out.print("Enter the course number: "); //prompts the user for the course number
      courseNumCheck = s.hasNextInt(); //checks if course num is a integer
 
      if(courseNumCheck){ //if the user actually enters a integer

        int courseNum = s.nextInt(); //gets input

      } //ends if statement

      else{ //if the user did not enter a integer

        System.out.println("Invalid, please try again");
        x = s.next(); //assigns bad input to random input

      } //ends else statement
    
    }while(!courseNumCheck); //loops while courseNumCheck = False (the user enters bad input)
    
    x = s.nextLine(); //because /n is in and messes up everything
    
    System.out.print("Enter the department name: "); //prompts user to enter the department name
    String depName = s.nextLine(); //gets dept name
    
    do{
    
      System.out.print("Enter the number of meetings: "); //prompts the user for the number of meetings
      meetingsCheck = s.hasNextInt(); //checks if meetings is a integer
 
      if(meetingsCheck){ //if the user actually enters a integer

        int meetings = s.nextInt(); //gets input

      } //ends if statement

      else{ //if the user did not enter a integer

        System.out.println("Invalid, please try again");
        x = s.next(); //assigns bad input to random input

      } //ends else statement
    
    }while(!meetingsCheck); //loops while meetingsCheck = False (the user enters bad input)
    
    x = s.nextLine(); //becuase /n is in and messes up everything
    
    do{
          
      System.out.print("Enter the military time the class starts (e.g. 1600): "); //prompts user for time
      timeCheck = s.hasNextInt(); //checks if time is a integer
 
      if(timeCheck){ //if the user actually enters a integer

        int timeClass = s.nextInt(); //gets input

      } //ends if statement

      else{ //if the user did not enter a integer

        System.out.println("Invalid, please try again");
        x = s.next(); //assigns bad input to random input

      } //ends else statement
    
    }while(!timeCheck); //loops while meetingsCheck = False (the user enters bad input)
    
    x = s.nextLine(); //because /n messes everything up
    
    System.out.print("Enter the instructor's name: "); //prompts user for instructor's name
    String insName = s.nextLine(); //gets instructors name from user
    
    do{
    
      System.out.print("Enter the number of students: "); //prompts the user for the number of students
      studentsCheck = s.hasNextInt(); //checks if students is a integer
 
      if(studentsCheck){ //if the user actually enters a integer

        int students = s.nextInt(); //gets input

      } //ends if statement

      else{ //if the user did not enter a integer

        System.out.println("Invalid, please try again");
        x = s.next(); //assigns bad input to random input

      } //ends else statement
    
    }while(!studentsCheck); //loops while meetingsCheck = False (the user enters bad input)
    
    
  } //ends the main method
  
} //ends the class