import java.util.Scanner; //imports scanner class necessary to get input

public class PatternC { //starts class necessary to run program

	public static void main(String[] args) { //main method required to run 
	
		int number = 0;
		boolean inputt = false;
		
		Scanner s = new Scanner(System.in); //creates scanner object to get input
		
		do {
			
			System.out.print("Enter number (1-10): "); //prompts user for input
			
			inputt = s.hasNextInt();
			
			if(inputt) {//if input is right
				int x = s.nextInt();
				if (x>0 && x<11) {
					number = x;
				}
				else {
					inputt = false;
				}
			}
			
			String trash = s.nextLine();
			
		}while (!inputt); //loops until boolean is an integer
		
		for(int i=number;i>=1;i--) {
			for(int j=1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int j=number-i+1;j>=1;j--) {
				System.out.print("" + j);
			}
			System.out.println("");
		}
		
	}//ends main method
	
} //ends class