///////////////////////////////////////
//Benjamin Aulenbach 11/1/18         //
//lab07.java                         //
//CSE 002, BHA222                    //
//Utilizes methods to perform tasks  //
///////////////////////////////////////

import java.util.Random; //imports the random class
import java.util.Scanner; //imports the scanner c

public class lab07 { //creates class needed for java program

	public static void main(String[] args) { //creates main method needed for java program
		
		Scanner s = new Scanner(System.in);
		boolean check = true;
		
		Random randGen = new Random(); //creates new instance of random
		
		while (check == true){ //creates a sentence
			String sentence = "The";
			int randInt = randGen.nextInt(10); //creates random number from 0-9
			sentence += " " + adj(randInt); //calls required method
			
			//same as previous two comments for this blockc
			
			randInt = randGen.nextInt(10);
			sentence += " " + nounSub(randInt);
			
			randInt = randGen.nextInt(10);
			sentence += " " + verb(randInt);
			sentence += " the";
			
			randInt = randGen.nextInt(10);
			sentence += " " + adj(randInt);
			
			randInt = randGen.nextInt(10);
			sentence += " " + nounSub(randInt);
			sentence += ".";
			
			System.out.println(sentence);
			System.out.print("Would you like another sentence? (y/n): ");
			String x = s.next();
			System.out.println(x);
			if (x.equals("n")) { //if they do not wnat another sentence
				check = false;
			}
		}
		
		secondSentence(); //create second sentence
		thirdSentence(); //create third sentence
		
	
	} //ends main method
	
	public static void secondSentence() { //creates secondSentence method
		
		Random randGen = new Random(); //creates new instance of random
		
		String sentence = "It "; //creates new variable
		
		sentence += adj(randGen.nextInt(10));
		sentence += " " + adj(randGen.nextInt(10));
		sentence += " " + nounObj(randGen.nextInt(10));
		sentence += " and " + adj(randGen.nextInt(10));
		sentence += " " + adj(randGen.nextInt(10));
		sentence += " " + nounObj(randGen.nextInt(10));
		sentence += ".";
		
		System.out.println(sentence);
		
	}
	
	public static void thirdSentence() { //creates thirdSentence method
		
		Random randGen = new Random(); //creates new instance of random
		
		String sentence = "It "; //creates new variable
		
		sentence += adj(randGen.nextInt(10));
		sentence += " its " + nounObj(randGen.nextInt(10));
		sentence += ".";
		
		System.out.println(sentence);
		
	}
	
	public static String adj(int num) { //creates adjective method
		
		String fin = "";
		
		switch (num) { //randomly generates a word using switch statement
			case 0:
				fin = "good";
				break;
			case 1:
				fin = "first";
				break;
			case 2:
				fin = "last";
				break;
			case 3:
				fin = "little";
				break;
			case 4:
				fin = "right";
				break;
			case 5:
				fin = "different";
				break;
			case 6:
				fin = "bad";
				break;
			case 7:
				fin = "same";
				break;
			case 8:
				fin = "young";
				break;
			case 9:
				fin = "few";
				break;
		}
		
		return fin;
		
	} //ends adj method
	
	public static String nounSub(int num) { //creates non-primary nouns method for subject
		
		String fin = "";
		
		switch(num) {
			case 0:
				fin = "favorite";
				break;
			case 1:
				fin = "friend";
				break;
			case 2:
				fin = "bunch";
				break;
			case 3:
				fin = "stack";
				break;
			case 4:
				fin = "train";
				break;
			case 5:
				fin = "book";
				break;
			case 6:
				fin = "brother";
				break;
			case 7:
				fin = "sister";
				break;
			case 8:
				fin = "cousin";
				break;
			case 9:
				fin = "laptop";
				break;
		}
		
		return fin;
		
	} //ends nounSub method
	
	public static String verb(int num) { //creates verb method
		
		String fin = "";
		
		switch(num) { //creates a random word using a switch statement
			case 0:
				fin = "leave";
				break;
			case 1:
				fin = "study";
				break;
			case 2:
				fin = "sit";
				break;
			case 3:
				fin = "eat";
				break;
			case 4:
				fin = "teach";
				break;
			case 5:
				fin = "manage";
				break;
			case 6:
				fin = "climb";
				break;
			case 7:
				fin = "cost";
				break;
			case 8:
				fin = "go";
				break;
			case 9:
				fin = "pay";
				break;			
		}
		
		return fin;
		
	} //ends verb method
	
	public static String nounObj(int num) { //creates non-primary nouns method for object
		
		String fin = "";
		
		switch(num) {
			case 0:
				fin = "computer";
				break;
			case 1:
				fin = "phone";
				break;
			case 2:
				fin = "tablet";
				break;
			case 3:
				fin = "headphones";
				break;
			case 4:
				fin = "pencil";
				break;
			case 5:
				fin = "keyboard";
				break;
			case 6:
				fin = "trackpad";
				break;
			case 7:
				fin = "dorm";
				break;
			case 8:
				fin = "library";
				break;
			case 9:
				fin = "classroom";
				break;
		}
		
		return fin;
		
	} //ends nounObj method
	
} //closes class