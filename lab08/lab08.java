////////////////////////////////////////
// lab08.java                         //
// CSE 002, BHA222                    //
// Practice utilizing arrays          //
////////////////////////////////////////

import java.util.Random; //imports the Random class neccessary to generate random number

public class lab08 { //initializes class necessary to run java program
	
	public static void main(String[] args) { //initializes main method necessary to run java program
	
		Random rand = new Random(); //creates new instance of random
		
		int[] array1 = new int[100]; //initializes an integer array of size 100
		int[] array2 = new int[100]; //initializes an integer array of size 100
		
		for(int i=0; i<array1.length; i++) { //goes through every item in array1 and gives it a random value
		
			array1[i] = rand.nextInt(100); //gets random number from 0-99
			array2[array1[i]] += 1; //adds one to the place of the randomly generated value
			
		} //closes for loop
		
		//print the output
		System.out.print("Array 1 holds the following integers:");
		
		for (int i=0; i<array1.length; i++) { //go through each item and print it
			System.out.print(" " + array1[i] );
		} //ends for loop
		
		System.out.println("\n");
		
		for (int i=0; i<array2.length; i++) { //go through each item and print it
			if (array2[i] != 1) { //to get grammar correct
				System.out.println("" + i + " occurs " + array2[i] + " times");
			}
			else {
				System.out.println("" + i + " occurs " + array2[i] + " time");
			}
			
		} //ends for loop
		
	} //ends main method
	
} //ends class