//////////////////////////////////////
// Benjamin Aulenbach 12/04/18      //
// lab09.java                       //
// CSE 002, BHA222                  //
// Practice utilizing arrays        //
//////////////////////////////////////

import java.util.Arrays;

public class lab09 { //creates class needed to run java program

	public static void main (String[] args) {  //creates main method needed to run java program
		
		int[] array0 = {1,2,3,4,5,6,7,8}; //initializes array
		
		int[] array1 = copy(array0); //creates copy of array
		int[] array2 = copy(array0); //creates copy of array
		
		inverter(array0); //inverts array0
		print(array0); //prints array0
		
		print(inverter2(array1)); //inverts and prints array1
		
		int[] array3 = inverter2(array2); //inverts array2 and assigns it to array3
		print(array3); //prints array3
		
	} //ends main method
	
	public static int[] copy(int[] list) { //creates copy method
		
		int[] copy = new int[list.length]; //creates new integer array same length as input
		
		for (int i=0; i<list.length; i++) { //loops through each element in the array
			copy[i] = list[i]; //copies the element in the 'list' to 'copy'
		} //ends for loop
		
		return copy; //returns the copied list
		
	} //ends copy method

	public static void inverter(int[] list) { //creates inverter method
		
		for (int i=0; i<(list.length)/2; i++) { //loops through every element in the list
			int temp = list[i]; //initializes a temp variable to swap elements
			list[i] = list[(list.length-1)-i]; //puts the last element minus i into the i element
			list[(list.length-1)-i] = temp; //uses the temp value to finish the swap
		} //ends for loop
		
	} //ends inverter method
	
	public static int[] inverter2(int[] list) { //creates inverter2 method
		
		int[] listCopy = copy(list); //creates a copy of the list using copy method
		
		for (int i=0; i<(listCopy.length)/2; i++) { //loops through every element in the list
			int temp = listCopy[i]; //initializes a temp variable to swap elements
			listCopy[i] = listCopy[(listCopy.length-1)-i]; //puts the last element minus i into the i element
			listCopy[(listCopy.length-1)-i] = temp; //uses the temp value to finish the swap
		} //ends for loop
		
		return listCopy; //returns the copied, inverted list

	} //ends inverter2 method
	
	public static void print(int[] list) { //creates print method
		
		for (int i=0; i<list.length; i++) { //loops through every element of the list
			System.out.print("" + list[i] + " "); //prints the element at index 'i'
		} //ends for loop
		
		System.out.println(); //goes to next line
		
	} //ends print method
	
} //ends class