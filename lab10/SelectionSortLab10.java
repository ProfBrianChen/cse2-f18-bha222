import java.util.Arrays; //imports Arrays class

//code given on lab spec
public class SelectionSortLab10 { //initializes SelectionSortLab10 class

	public static void main(String[] args) { //initializes main method
		
		int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
		int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	
	} //ends main method
	
	//The method for sorting the numbers
	public static int selectionSort(int[] list) {
		//prints the initial array (you must insert another
		//print out statement later in the code to show the
		//array as it's being sorted)
		System.out.println(Arrays.toString(list));
		//Initialize counter for iterations
		int iterations = 0;

		for (int i=0; i<list.length-1; i++) {
			//Update the iterations counter
			iterations++;

			//Step One: FInd the minimum in the list[i..list.length-1]
			int currentMin = list[i];
			int currentMinIndex = i;
			for (int j = i+1; j<list.length; j++) {
				iterations++;
				if (list[currentMinIndex] > list[j]) {
					currentMinIndex = j;
				}
				//COMPLETE THIS FOR LOOP
				// (In this step, make sure you update the iteraiton
				//counter time you compare the current min to another
				//element in the array
			}

			//Step Two: Swap list[i] with the minimum you found above
			if (currentMinIndex != i) {
				int temp = list[currentMinIndex];
				list[currentMinIndex] = list[i];
				list[i] = temp;
				System.out.println(Arrays.toString(list));
			}
			
		}
		return iterations;
	}
		
} //ends main method